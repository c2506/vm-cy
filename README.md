# vm-cy

Tested on macOS 11.6 (Big Sur).

#### Manual setup

In a terminal, run the following:

```bash
git clone git@gitlab.com:c2506/vm-cy.git
cd vm-cy
yarn install
```

#### Run tests in Chrome 94 (headless)

```bash
yarn cypress run --browser chrome
```

#### Develop tests

```bash
yarn cypress open
```

#### GitLab pipeline

Mochawesome report is attached as an artifact by the [chrome-tests](https://gitlab.com/c2506/vm-cy/-/jobs) job.
