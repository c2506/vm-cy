const getDateOfBirth = ({ age }) => {
  const date = new Date();
  date.setFullYear(date.getFullYear() - age);
  return [
    date.getDate().toString(),
    (date.getMonth() + 1).toString(),
    date.getFullYear().toString()
  ];
};

export { getDateOfBirth };
