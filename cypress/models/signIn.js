const footer = '.page__content--secondary';

const selectors = {
  form: '.page__content--flex',
  footer,
  registrationLink: `${footer} > .link > span`,
  identifierInput: '#user-identifier-input',
  identifierInputMessage: '#form-message-username',
  passwordInput: '#password-input',
  passwordInputMessage: '#form-message-password'
};

export { selectors };
