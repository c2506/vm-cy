const selectors = {
  overAgeLink: '[data-bbc-result="/register/details/age"]',
  dayInput: '#day-input',
  monthInput: '#month-input',
  yearInput: '#year-input',
  submitButton: '#submit-button',
  locationSelect: '#location-select'
};

export { selectors };
