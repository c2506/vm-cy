/// <reference types="cypress" />
import { selectors } from '../models/signIn';

describe('Sign in page', () => {
  beforeEach(() => {
    cy.visit('https://session.test.bbc.co.uk/session?lang=en-GB');
  });

  it('should render the expected form and footer', () => {
    cy.injectAxe();
    cy.checkA11y(null, {
      includedImpacts: ['critical']
    });

    cy.get(selectors.form).toMatchImageSnapshot({
      imageConfig: {
        threshold: 2
      }
    });

    cy.get(selectors.footer).toMatchImageSnapshot({
      imageConfig: {
        threshold: 2
      }
    });
  });

  it('should show error messages when the identifier and password inputs are empty, focused and blurred', () => {
    cy.injectAxe();

    cy.get(selectors.identifierInput)
      .focus()
      .blur();
    cy.get(selectors.identifierInputMessage).should('be.visible');
    cy.get(selectors.passwordInput)
      .focus()
      .blur();
    cy.get(selectors.passwordInputMessage).should('be.visible');

    cy.checkA11y(null, {
      includedImpacts: ['critical']
    });

    cy.get(selectors.form).toMatchImageSnapshot({
      imageConfig: {
        threshold: 2
      }
    });
  });

  it('should show error messages when the identifier is invalid and the input is blurred', () => {
    cy.fixture('signIn/identifierValidation.json').then(fixture => {
      fixture.forEach(({ value, message }) => {
        cy.get(selectors.identifierInput)
          .type(value)
          .blur();

        cy.get(selectors.identifierInputMessage).should('have.text', message);

        cy.get(selectors.identifierInput).clear();
      });
    });
  });
});
