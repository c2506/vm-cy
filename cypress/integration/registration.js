/// <reference types="cypress" />
import { getDateOfBirth } from '../common/date';
import { selectors as signInSelectors } from '../models/signIn';
import { selectors as registrationSelectors } from '../models/registration';

describe('Registration journey', () => {
  beforeEach(() => {
    cy.visit('https://session.test.bbc.co.uk/session?lang=en-GB');
  });

  it("should set the Country select to an international user's country", () => {
    cy.setCookie('ckns_location', 'US');
    const expectedCountry = 'United States of America';

    cy.get(signInSelectors.registrationLink).click();

    cy.get(registrationSelectors.overAgeLink).click();

    const [day, month, year] = getDateOfBirth({ age: 16 });
    cy.get(registrationSelectors.dayInput).type(day);
    cy.get(registrationSelectors.monthInput).type(month);
    cy.get(registrationSelectors.yearInput).type(year);

    cy.get(registrationSelectors.submitButton).click();

    cy.get(registrationSelectors.locationSelect)
      .find('option:selected')
      .should('have.text', expectedCountry);
  });
});
